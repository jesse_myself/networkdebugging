//
//  URLTestVC.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/9.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Alamofire

class URLTestVC: GeneralVC {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false

        let mainView = URLTestView(frame: BOUNDS)
        self.bv.addSubview(mainView)
        mainView.testButtonActionClosure { (url: String, params: [String : String]) in
            let vc = URLTestResultVC()
            vc.url = url
            vc.params = params
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

