//
//  URLTestResultVC.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/16.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Alamofire

class URLTestResultVC: UIViewController {

    var url = String()
    var params = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        
        let mainView = URLTestResultView(frame: BOUNDS)
        self.view.addSubview(mainView)
        
        self.testTheUrl { (response: DataResponse<Any>) in
            mainView.getTheTimes(response: response)
        }

        // Do any additional setup after loading the view.
    }

    
    func testTheUrl(getResponse:@escaping (_ response: DataResponse<Any>)-> Void) -> Void {
        print("url___\(url)")
        print("params___\(params)")
        let headers: HTTPHeaders = [
            "Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
            "Accept": "application/json"
        ]
        let urlRequest = Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers)
        urlRequest.responseJSON(completionHandler: { (response: DataResponse<Any>) in
            getResponse(response)
        })
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
