//
//  URLTestResultView.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/16.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Masonry
import Alamofire


class URLTestResultView: UIView, UITextViewDelegate {
    
    var LatencyTitleLabel: UILabel!
    var RequestDurationTitleLabel: UILabel!
    var SerializationDuratioTitleLabel: UILabel!
    var TotalDurationTitleLabel: UILabel!
    
    var LatencyLabel: UILabel!
    var RequestDurationLabel: UILabel!
    var SerializationDuratioLabel: UILabel!
    var TotalDuration: UILabel!
    
    var jsonTextView: UITextView!

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.gray
        
        LatencyTitleLabel = UILabel()
        LatencyTitleLabel.text = "  延迟时间："
        LatencyTitleLabel.layer.cornerRadius = 2.0
        LatencyTitleLabel.layer.borderColor = UIColor.black.cgColor
        LatencyTitleLabel.layer.borderWidth = 1.0
        RequestDurationTitleLabel = UILabel()
        RequestDurationTitleLabel.text = "  请求总时间："
        RequestDurationTitleLabel.layer.cornerRadius = 2.0
        RequestDurationTitleLabel.layer.borderColor = UIColor.black.cgColor
        RequestDurationTitleLabel.layer.borderWidth = 1.0
        SerializationDuratioTitleLabel = UILabel()
        SerializationDuratioTitleLabel.text = "  序列化时间："
        SerializationDuratioTitleLabel.layer.cornerRadius = 2.0
        SerializationDuratioTitleLabel.layer.borderColor = UIColor.black.cgColor
        SerializationDuratioTitleLabel.layer.borderWidth = 1.0
        TotalDurationTitleLabel = UILabel()
        TotalDurationTitleLabel.text = "  总时间："
        TotalDurationTitleLabel.layer.cornerRadius = 2.0
        TotalDurationTitleLabel.layer.borderColor = UIColor.black.cgColor
        TotalDurationTitleLabel.layer.borderWidth = 1.0
        
        LatencyLabel = UILabel()
        LatencyLabel.layer.cornerRadius = 2.0
        LatencyLabel.layer.borderColor = UIColor.black.cgColor
        LatencyLabel.layer.borderWidth = 1.0
        RequestDurationLabel = UILabel()
        RequestDurationLabel.layer.cornerRadius = 2.0
        RequestDurationLabel.layer.borderColor = UIColor.black.cgColor
        RequestDurationLabel.layer.borderWidth = 1.0
        SerializationDuratioLabel = UILabel()
        SerializationDuratioLabel.layer.cornerRadius = 2.0
        SerializationDuratioLabel.layer.borderColor = UIColor.black.cgColor
        SerializationDuratioLabel.layer.borderWidth = 1.0
        TotalDuration = UILabel()
        TotalDuration.layer.cornerRadius = 2.0
        TotalDuration.layer.borderColor = UIColor.black.cgColor
        TotalDuration.layer.borderWidth = 1.0
        
        jsonTextView = UITextView()
        jsonTextView.delegate = self
        jsonTextView.layer.cornerRadius = 2.0
        jsonTextView.layer.borderColor = UIColor.black.cgColor
        jsonTextView.layer.borderWidth = 1.0
        
        self.addSubview(LatencyTitleLabel)
        self.addSubview(RequestDurationTitleLabel)
        self.addSubview(SerializationDuratioTitleLabel)
        self.addSubview(TotalDurationTitleLabel)
        self.addSubview(LatencyLabel)
        self.addSubview(RequestDurationLabel)
        self.addSubview(SerializationDuratioLabel)
        self.addSubview(TotalDuration)
        self.addSubview(jsonTextView)
        
        LatencyTitleLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(35.0)
            maker?.leading.equalTo()(self)?.setOffset(25.0)
            maker?.width.setOffset(120.0)
            maker?.height.setOffset(35.0)
        }
        LatencyLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(35.0)
            maker?.leading.equalTo()(self.LatencyTitleLabel.mas_trailing)?.setOffset(25.0)
            maker?.trailing.equalTo()(self)?.setOffset(-25.0)
            maker?.height.setOffset(35.0)
        }
        
        RequestDurationTitleLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.LatencyTitleLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self)?.setOffset(25.0)
            maker?.width.setOffset(120.0)
            maker?.height.setOffset(35.0)
        }
        RequestDurationLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.LatencyLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self.RequestDurationTitleLabel.mas_trailing)?.setOffset(25.0)
            maker?.trailing.equalTo()(self)?.setOffset(-25.0)
            maker?.height.setOffset(35.0)
        }
        
        SerializationDuratioTitleLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.RequestDurationTitleLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self)?.setOffset(25.0)
            maker?.width.setOffset(120.0)
            maker?.height.setOffset(35.0)
        }
        SerializationDuratioLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.RequestDurationLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self.SerializationDuratioTitleLabel.mas_trailing)?.setOffset(25.0)
            maker?.trailing.equalTo()(self)?.setOffset(-25.0)
            maker?.height.setOffset(35.0)
        }
        
        TotalDurationTitleLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.SerializationDuratioTitleLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self)?.setOffset(25.0)
            maker?.width.setOffset(120.0)
            maker?.height.setOffset(35.0)
        }
        TotalDuration.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.SerializationDuratioLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self.TotalDurationTitleLabel.mas_trailing)?.setOffset(25.0)
            maker?.trailing.equalTo()(self)?.setOffset(-25.0)
            maker?.height.setOffset(35.0)
        }
        
        jsonTextView.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.TotalDurationTitleLabel.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self)?.setOffset(0.0)
            maker?.trailing.equalTo()(self)?.setOffset(0.0)
            maker?.bottom.equalTo()(self)?.setOffset(-64.0)
        }
        
    }
    
    func getTheTimes(response: DataResponse<Any>) -> Void {
//        print("rr__\(urlRequest)")
//        print("time_\(response.timeline)")
        let latency = String(format: "  %.4f", response.timeline.latency) + "s"
        let requestDuration = String(format: "  %.4f", response.timeline.requestDuration) + "s"
        let serializationDuration = String(format: "  %.4f", response.timeline.serializationDuration) + "s"
        let totalDuration = String(format: "  %.4f", response.timeline.totalDuration) + "s"

        self.LatencyLabel.text = latency
        self.RequestDurationLabel.text = requestDuration
        self.SerializationDuratioLabel.text = serializationDuration
        self.TotalDuration.text = totalDuration
        
        if let JSON = response.result.value {
            self.jsonTextView.text = "\(JSON)"
        }
        
        if let error = response.error {
            print("ERROR: \(error)")
        }

        
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //监听回车键
        if text == "\n" {
            self.endEditing(true)
            return false
        }
        return true
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
