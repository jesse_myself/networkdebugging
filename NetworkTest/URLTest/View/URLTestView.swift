//
//  URLTestView.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/14.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Masonry

class URLTestView: UIView, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    var urlTextfield: UITextField!
    var paramsTableView: UITableView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let testButton = UIButton()
        self.addSubview(testButton)
        testButton.setTitle("提交", for: UIControlState.normal)
        testButton.setTitleColor(oooooo, for: UIControlState.normal)
        testButton.addTarget(self, action: #selector(URLTestView.testButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        testButton.layer.borderColor = UIColor.black.cgColor
        testButton.layer.borderWidth = 1
        testButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(35.0)
            maker?.trailing.equalTo()(self)?.setOffset(-12.0)
            maker?.width.setOffset(65.0)
            maker?.height.setOffset(45.0)
        }
        
        urlTextfield = UITextField()
        self.addSubview(urlTextfield)
        urlTextfield.placeholder = "  请输入url"
        urlTextfield.delegate = self
        urlTextfield.layer.borderColor = UIColor.black.cgColor
        urlTextfield.layer.borderWidth = 1
        urlTextfield.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(35.0)
            maker?.leading.equalTo()(self)?.setOffset(12.0)
            maker?.trailing.equalTo()(testButton.mas_leading)?.setOffset(-12.0)
            maker?.height.setOffset(45.0)
        }
        
        let clearButton = UIButton()
        self.addSubview(clearButton)
        clearButton.setTitle("清除", for: UIControlState.normal)
        clearButton.setTitleColor(oooooo, for: UIControlState.normal)
        clearButton.addTarget(self, action: #selector(URLTestView.clearButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        clearButton.layer.borderColor = UIColor.black.cgColor
        clearButton.layer.borderWidth = 1
        clearButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.urlTextfield.mas_bottom)?.setOffset(30.0)
            maker?.trailing.equalTo()(self)?.setOffset(-12.0)
            maker?.height.setOffset(45.0)
            maker?.width.setOffset(60.0)
        }
        
        let addButton = UIButton()
        self.addSubview(addButton)
        addButton.setTitle("添加参数", for: UIControlState.normal)
        addButton.setTitleColor(oooooo, for: UIControlState.normal)
        addButton.addTarget(self, action: #selector(URLTestView.addButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        addButton.layer.borderColor = UIColor.black.cgColor
        addButton.layer.borderWidth = 1
        addButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.urlTextfield.mas_bottom)?.setOffset(30.0)
            maker?.leading.equalTo()(self)?.setOffset(12.0)
            maker?.trailing.equalTo()(clearButton.mas_leading)?.setOffset(-12.0)
            maker?.height.setOffset(45.0)
        }
        
        paramsTableView = UITableView()
        self.addSubview(paramsTableView)
        paramsTableView.dataSource = self
        paramsTableView.delegate = self
        paramsTableView.backgroundColor = UIColor.gray
        paramsTableView.register(URLTestViewParamsTabeViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        paramsTableView.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(addButton.mas_bottom)?.setOffset(30.0)
            maker?.leading.equalTo()(self)?.setOffset(0.0)
            maker?.trailing.equalTo()(self)?.setOffset(0.0)
            maker?.bottom.equalTo()(self)?.setOffset(-64.0)
        }
        
        
    }
    
    
    //MARK: submit
    private var closure: urlAndParamsClosure?
    func testButtonActionClosure(sender: @escaping urlAndParamsClosure) -> Void {
        closure = sender
    }
    @objc private func testButtonAction(sender: UIButton) -> Void {
        if let confirm_closure = closure {
            var theParams = [String: String]()
            for index in 0..<params.count {
                let param = params[index]
                if param.keys.first! != "" {
                    theParams.updateValue(param.values.first!, forKey: param.keys.first!)
                }
            }
            confirm_closure(urlTextfield.text!, theParams)
        }
    }
    
    //MAKR: add
    @objc private func addButtonAction(sender: UIButton) -> Void {
        params.append(["": ""])
        let indexPaths = [IndexPath(row: params.count - 1, section: 0)]
        paramsTableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.left)
        print("all_p\(params)")
    }
    
    //MARK: clear
    func clearButtonAction(sender: UIButton) -> Void {
        var indexPaths = [IndexPath]()
        for index in 0..<params.count {
            let indexPath = IndexPath(row: index, section: 0)
            indexPaths.append(indexPath)
        }
        params.removeAll()
        paramsTableView.deleteRows(at: indexPaths, with: UITableViewRowAnimation.right)
    }
    
    //MARK: tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.params.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! URLTestViewParamsTabeViewCell
        cell.ParamsValuesChanged(keyChanged: { (key: String) in
            let theIndexPath = tableView.indexPath(for: cell)
            if let confirm_indexPath = theIndexPath {
                var dic = self.params[confirm_indexPath.row]
                var dicKey = dic.keys.first!
                let dicValue = dic.values.first!
                dicKey = key
                dic = [dicKey: dicValue]
                self.params.remove(at: confirm_indexPath.row)
                self.params.insert(dic, at: confirm_indexPath.row)
            }
        }, valueChaged: { (value: String) in
            let theIndexPath = tableView.indexPath(for: cell)
            if let confirm_indexPath = theIndexPath {
                var dic = self.params[confirm_indexPath.row]
                let dicKey = dic.keys.first!
                var dicValue = dic.values.first!
                dicValue = value
                dic = [dicKey: dicValue]
                self.params.remove(at: confirm_indexPath.row)
                self.params.insert(dic, at: confirm_indexPath.row)
            }
        }) {
            let theIndexPath = tableView.indexPath(for: cell)
            if let confirm_indexPath = theIndexPath {
                self.params.remove(at: confirm_indexPath.row)
                let indexPaths = [confirm_indexPath]
                self.paramsTableView.deleteRows(at: indexPaths, with: UITableViewRowAnimation.right)
            }
        }
        print(params)
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.params.count > indexPath.row {
            let dic = self.params[indexPath.row]
            (cell as! URLTestViewParamsTabeViewCell).setValuesWithDic(dic: dic)
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    
    var params = [[String(): String()]]  //参数
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
