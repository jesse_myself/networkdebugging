//
//  URLTestViewParamsTabeViewCell.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/16.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Masonry

class URLTestViewParamsTabeViewCell: UITableViewCell, UITextFieldDelegate {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var keyTextField: UITextField!
    var valueTextField: UITextField!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = UIColor.orange

        let deleteButton = UIButton()
        self.contentView.addSubview(deleteButton)
        deleteButton.setTitle("删除", for: UIControlState.normal)
        deleteButton.addTarget(self, action: #selector(URLTestViewParamsTabeViewCell.deleteButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        deleteButton.backgroundColor = UIColor.blue

        keyTextField = UITextField()
        self.contentView.addSubview(keyTextField)
        keyTextField.addTarget(self, action: #selector(URLTestViewParamsTabeViewCell.keyTextFieldValueChanged(sender:)), for: UIControlEvents.editingChanged)
        keyTextField.keyboardType = .URL
        keyTextField.delegate = self
        keyTextField.placeholder = "  key"
        keyTextField.layer.cornerRadius = 2
        keyTextField.layer.borderColor = UIColor.black.cgColor
        keyTextField.layer.borderWidth = 1
        
        valueTextField = UITextField()
        self.contentView.addSubview(valueTextField)
        valueTextField.addTarget(self, action: #selector(URLTestViewParamsTabeViewCell.valueTextFieldValueChanged(sender:)), for: UIControlEvents.editingChanged)

        valueTextField.keyboardType = .URL
        valueTextField.delegate = self
        valueTextField.placeholder = "  value"
        valueTextField.layer.cornerRadius = 2
        valueTextField.layer.borderColor = UIColor.black.cgColor
        valueTextField.layer.borderWidth = 1
        
        deleteButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.contentView)?.setOffset(0.0)
            maker?.trailing.equalTo()(self.contentView)?.setOffset(0.0)
            maker?.bottom.equalTo()(self.contentView)?.setOffset(0.0)
            maker?.width.setOffset(60.0)
        }
        
        valueTextField.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.contentView)?.setOffset(10.0)
            maker?.bottom.equalTo()(self.contentView)?.setOffset(-10.0)
            maker?.trailing.equalTo()(deleteButton.mas_leading)?.setOffset(-35.0)
        }
        
        keyTextField.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.contentView)?.setOffset(10.0)
            maker?.bottom.equalTo()(self.contentView)?.setOffset(-10.0)
            maker?.leading.equalTo()(self.contentView)?.setOffset(35.0)
            maker?.trailing.equalTo()(self.valueTextField.mas_leading)?.setOffset(-35.0)
            maker?.width.equalTo()(self.valueTextField.mas_width)
        }
        
    }
    // MARK: 监听key的变化
    private var keyChangedClosure: oneStringParamClosure?
    @objc private func keyTextFieldValueChanged(sender: UITextField) -> Void {
        if let confirm_closure = keyChangedClosure {
            confirm_closure(sender.text!)
        }
    }
    // MARK: 监听value的变化
    private var valueChangedClosure: oneStringParamClosure?
    @objc private func valueTextFieldValueChanged(sender: UITextField) -> Void {
        if let confirm_closure = valueChangedClosure {
            confirm_closure(sender.text!)
        }
    }
    
    private var deleteClosure: notParamsClosure?
    //MARK: 删除按钮
    @objc private func deleteButtonAction(sender: UIButton) -> Void {
        if let confirm_closure = deleteClosure {
            confirm_closure()
        }
    }
    
    func ParamsValuesChanged(keyChanged: @escaping oneStringParamClosure, valueChaged: @escaping oneStringParamClosure, delete: @escaping notParamsClosure) -> Void {
        keyChangedClosure = keyChanged
        valueChangedClosure = valueChaged
        deleteClosure = delete
    }
    
    
    func setValuesWithDic(dic: [String: String]) -> Void {
        guard let key = dic.keys.first else {
            return
        }
        guard let value = dic.values.first else {
            return
        }
        self.keyTextField.text = key
        self.valueTextField.text = value
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.contentView.endEditing(true)
        return true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
