//
//  GeneralRequest.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/8.
//  Copyright © 2017年 vsea. All rights reserved.
//



import UIKit
import Alamofire


class GeneralRequest: NSObject {
    
    
    
    class func requestWithUrl(url: String, getResponse: @escaping (_ response: DataResponse<Any>) -> Void) -> Void {
        
        let dataRequest = request(url)
        dataRequest.responseJSON { (response: DataResponse<Any>) in
            print(dataRequest.progress)
            getResponse(response)
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    

}
