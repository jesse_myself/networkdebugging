//
//  GeneralVC.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/8.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Alamofire

class GeneralVC: UIViewController {
    
    var bv: GeneralView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bv = GeneralView(frame: BOUNDS)
        self.view.addSubview(bv)
        // Do any additional setup after loading the view.
    }
    
    
    func requestDatas(url: String, getResponse: @escaping (_ response: DataResponse<Any>) -> Void) -> Void {
        bv.beginAnimation()
        GeneralRequest.requestWithUrl(url: url) { (response: DataResponse<Any>) in
            if let error = response.error {
                self.bv.requestError()
                print("request_error_\(error)")
            } else {
                self.bv.endAnimation()
                getResponse(response)
            }
        }
    }
    
    

    
    
    
    
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



