//
//  GeneralView.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/8.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit

import Masonry

class GeneralView: UIView {
    
    var errorView: UIView!
    var normalView: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initErrorView()
        self.initNormalView()
    }
    
    
    //MARK:
    func initErrorView() -> Void {
        errorView = UIView(frame: BOUNDS)
        self.addSubview(errorView)
        errorView.backgroundColor = FFFFFF
        
        let error_image = UIImageView()
        errorView.addSubview(error_image)
        error_image.image = UIImage(named: "network_error")
        error_image.backgroundColor = UIColor.black
        
        error_image.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(100.0)
            maker?.centerX.equalTo()(self.errorView)
            maker?.height.setOffset(128.0)
            maker?.width.setOffset(128.0)
        }
        
        let error_label = UILabel()
        errorView.addSubview(error_label)
        error_label.backgroundColor = UIColor.red
        error_label.text = "网络错误！"
        error_label.textColor = UIColor.green
        error_label.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(error_image.mas_bottom)?.setOffset(40.0)
            maker?.centerX.equalTo()(self.errorView)
            maker?.height.setOffset(20.0)
        }
        
        let error_button = UIButton()
        error_button.backgroundColor = UIColor.green
        errorView.addSubview(error_button)
        error_button.setTitleColor(oooooo, for: UIControlState.normal)
        error_button.setTitle("界面刷新", for: UIControlState.normal)
        error_button.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(error_label.mas_bottom)?.setOffset(60.0)
            maker?.leading.equalTo()(self.errorView)?.setOffset(38.0)
            maker?.trailing.equalTo()(self.errorView)?.setOffset(-38.0)
            maker?.height.setOffset(45.0)
        }
    }
    
    //MARK:
    func initNormalView() -> Void {
        normalView = UIView(frame: BOUNDS)
        normalView.backgroundColor = FFFFFF
        self.addSubview(normalView)
    }
    
    
    var shapelayer: CAShapeLayer!
    //MARK: 开始loading动画
    func beginAnimation() -> Void {
        self.bringSubview(toFront: normalView)
        
        shapelayer = CAShapeLayer()
        //arcCenter必须为(0, 0)
        let bezierPath = UIBezierPath(arcCenter: CGPoint(x: 0, y: 0), radius: 30, startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: true)
        shapelayer.path = bezierPath.cgPath
        shapelayer.fillColor = UIColor.clear.cgColor
        shapelayer.strokeColor = UIColor.red.cgColor
        shapelayer.lineWidth = 2.0
        shapelayer.position = CGPoint(x: WIDTH / 2, y: HEIGHT / 2)
        self.layer.addSublayer(shapelayer)
        
        let strokeAnimation = CABasicAnimation(keyPath: "strokeEnd")
        strokeAnimation.fromValue = 0.0
        strokeAnimation.toValue = 0.999
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = M_PI * 0
        rotateAnimation.toValue = M_PI * 2
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [strokeAnimation, rotateAnimation]
        groupAnimation.repeatCount = HUGE
        groupAnimation.duration = 1.0
        
        let colorAnimation = CAKeyframeAnimation(keyPath: "strokeColor")
        colorAnimation.values = [UIColor.red.cgColor, UIColor.blue.cgColor, UIColor.yellow.cgColor, UIColor.black.cgColor]
        colorAnimation.repeatCount = HUGE
        colorAnimation.duration = 6
        
        shapelayer.add(groupAnimation, forKey: nil)
        shapelayer.add(colorAnimation, forKey: nil)
        
    }
    
    //MARK: 结束loading动画
    func endAnimation() -> Void {
        self.shapelayer.removeFromSuperlayer()
    }
    
    
    //MARK: 网络错误
    func requestError() -> Void {
        self.bringSubview(toFront: errorView)
    }
    
    
    func addSubviewCustom(view: UIView) -> Void {
        self.normalView.addSubview(view)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

















