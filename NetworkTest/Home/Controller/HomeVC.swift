//
//  HomeVC.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/8.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Alamofire



class HomeVC: GeneralVC {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        
        let mainView = HomeView(frame: BOUNDS)
        mainView.backgroundColor = UIColor.red
        self.bv.addSubviewCustom(view: mainView)
        //MARK: url
        mainView.urlTestButtonActionClosure {
            let vc = URLTestVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        //MARK: ping
        mainView.pingTestButtonActionClosure {
            let vc = PingVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        mainView.downloadTestButtonActionClosure {
            let vc = DownloadTestVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
//        self.requestDatas(url: "http://35.167.29.54:8181/v1/home") { (response: DataResponse<Any>) in
////            print("value__\(response.value)")
//        }
        
        
        
        
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
