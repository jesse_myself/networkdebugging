//
//  HomeView.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/8.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit

import Masonry



class HomeView: UIView {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let urlTestButton = UIButton()
        self.addSubview(urlTestButton)
        urlTestButton.setTitle("接口测试", for: UIControlState.normal)
        urlTestButton.addTarget(self, action: #selector(HomeView.urlTestButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        urlTestButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(100.0)
            maker?.leading.equalTo()(self)?.setOffset(45.0)
            maker?.trailing.equalTo()(self)?.setOffset(-45.0)
            maker?.height.setOffset(45.0)
        }
        
        let pingTestButton = UIButton()
        self.addSubview(pingTestButton)
        pingTestButton.setTitle("ping", for: UIControlState.normal)
        pingTestButton.addTarget(self, action: #selector(HomeView.pingTestButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        pingTestButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(urlTestButton.mas_bottom)?.setOffset(45.0)
            maker?.leading.equalTo()(self)?.setOffset(45.0)
            maker?.trailing.equalTo()(self)?.setOffset(-45.0)
            maker?.height.setOffset(45.0)
        }
        
        let downloadTestButton = UIButton()
        self.addSubview(downloadTestButton)
        downloadTestButton.setTitle("下载速度测试", for: UIControlState.normal)
        downloadTestButton.addTarget(self, action: #selector(HomeView.downloadTestButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        downloadTestButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(pingTestButton.mas_bottom)?.setOffset(45.0)
            maker?.leading.equalTo()(self)?.setOffset(45.0)
            maker?.trailing.equalTo()(self)?.setOffset(-45.0)
            maker?.height.setOffset(45.0)
        }
        
        
        
        
    }
    
    
    private var urlClosure: notParamsClosure?
    func urlTestButtonActionClosure(sender: @escaping notParamsClosure) -> Void {
        self.urlClosure = sender
    }
    //MARK: url
    @objc private func urlTestButtonAction(sender: UIButton) -> Void {
        if let confirm_closure = urlClosure {
            confirm_closure()
            
        }
    }
    
    
    private var pingClosure: notParamsClosure?
    func pingTestButtonActionClosure(sender: @escaping notParamsClosure) -> Void {
        pingClosure = sender
    }
    //MARK: PING
    @objc private func pingTestButtonAction(sender: UIButton) -> Void {
        if let confirm_closure = pingClosure {
            confirm_closure()
        }
        
    }
    
    private var downloadTestClosure: notParamsClosure?
    func downloadTestButtonActionClosure(sender: @escaping notParamsClosure) -> Void {
        downloadTestClosure = sender
    }
    //MARK: download
    @objc private func downloadTestButtonAction(sender: UIButton) -> Void {
        if let confirm_closure = downloadTestClosure {
            confirm_closure()
        }
    }
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    
    
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
