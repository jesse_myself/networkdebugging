//
//  PingViewDetailsCell.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/17.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Masonry

class PingViewDetailsCell: UITableViewCell {
    
    var textsLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        textsLabel = UILabel()
        self.contentView.addSubview(textsLabel)
        textsLabel.numberOfLines = 0
        textsLabel.layer.cornerRadius = 2
        textsLabel.layer.borderColor = UIColor.black.cgColor
        textsLabel.layer.borderWidth = 1
        
        textsLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.contentView)?.setOffset(5.0)
            maker?.leading.equalTo()(self.contentView)?.setOffset(5.0)
            maker?.trailing.equalTo()(self.contentView)?.setOffset(-5.0)
            maker?.bottom.equalTo()(self.contentView)?.setOffset(-5.0)
        }
        
    }
    
    func setValueWithStr(str: String) -> Void {
        textsLabel.text = str
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
