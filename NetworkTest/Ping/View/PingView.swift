//
//  PingView.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/17.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Masonry

class PingView: UIView, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    var urlTextField: UITextField!
    var confirmButton: UIButton!
    
    var pingTableView: UITableView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        urlTextField = UITextField()
        self.addSubview(urlTextField)
        urlTextField.delegate = self
        urlTextField.placeholder = "请输入IP地址或域名"
        urlTextField.text = "http://www.vsea.com.cn/"
        urlTextField.layer.cornerRadius = 2
        urlTextField.layer.borderColor = UIColor.black.cgColor
        urlTextField.layer.borderWidth = 1
        
        confirmButton = UIButton()
        self.addSubview(confirmButton)
        confirmButton.layer.cornerRadius = 2
        confirmButton.layer.borderColor = UIColor.black.cgColor
        confirmButton.layer.borderWidth = 1
        confirmButton.setTitle("Ping", for: UIControlState.normal)
        confirmButton.addTarget(self, action: #selector(PingView.confirmButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        
        pingTableView = UITableView()
        self.addSubview(pingTableView)
        pingTableView.dataSource = self
        pingTableView.delegate = self
        pingTableView.separatorStyle = .none
        pingTableView.rowHeight = UITableViewAutomaticDimension
        pingTableView.estimatedRowHeight = 80.0
        pingTableView.register(PingViewDetailsCell.classForCoder(), forCellReuseIdentifier: "cell")
        
        urlTextField.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(15.0)
            maker?.leading.equalTo()(self)?.setOffset(15.0)
            maker?.trailing.equalTo()(self.confirmButton.mas_leading)?.setOffset(-15.0)
            maker?.height.setOffset(35.0)
        }
        
        confirmButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(15.0)
            maker?.trailing.equalTo()(self)?.setOffset(-15.0)
            maker?.width.setOffset(65.0)
            maker?.height.setOffset(35.0)
        }
        
        pingTableView.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.urlTextField.mas_bottom)?.setOffset(15.0)
            maker?.leading.equalTo()(self)?.setOffset(0.0)
            maker?.trailing.equalTo()(self)?.setOffset(0.0)
            maker?.bottom.equalTo()(self)?.setOffset(0.0)
        }

        
        
        
    }
    
    var pingServer: STDPingServices!  //防止被释放
    var pingStatus = Bool()  //记录ping的开启状态
    //MARK: Ping 按钮事件
    func confirmButtonAction(sender: UIButton) -> Void {
        //开始Ping
        if pingStatus == false {
            self.dataSource.removeAll()
            self.pingTableView.reloadData()
            confirmButton.setTitle("Cancel", for: UIControlState.normal)
            pingStatus = !pingStatus
            pingServer = STDPingServices.startPingAddress(urlTextField.text!) { [weak self] (pingItem: STDPingItem?, pingitems: [Any]?) in
                
                
                guard let confirm_pingItem = pingItem else {
                    return
                }
                //Ping持续进行中
                if confirm_pingItem.status != .finished {
                    self?.dataSource.append(confirm_pingItem.description)
                    let indexPaths = [IndexPath(row: 0, section: 0)]
                    self?.pingTableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.bottom)
                } else {
                    //取消Ping后回掉
                    self?.pingServer = nil
                    self?.dataSource.append(STDPingItem.statistics(withPingItems: pingitems))
                    let indexPaths = [IndexPath(row: 0, section: 0)]
                    self?.pingTableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.bottom)
                    self?.confirmButton.setTitle("Ping", for: UIControlState.normal)
                    self?.pingStatus = !(self?.pingStatus)!
                }
            }
            
            
        } else {
            //取消Ping
            pingServer.cancel()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PingViewDetailsCell
        if dataSource.count - indexPath.row > 0 {
            let index = dataSource.count - indexPath.row - 1
            if dataSource.count > index {
                let str = dataSource[index]
                cell.setValueWithStr(str: str)
            }
        }
        return cell
    }
    
    var dataSource = [String]()
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
