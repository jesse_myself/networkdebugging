//
//  DownloadTestVC.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/21.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit

class DownloadTestVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mainView = DownloadTestView(frame: BOUNDS)
        self.view.addSubview(mainView)

        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
