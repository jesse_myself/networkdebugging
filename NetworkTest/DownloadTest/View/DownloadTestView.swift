//
//  DownloadTestView.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/21.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Masonry
import Alamofire

class DownloadTestView: UIView {
    
    var testButton: UIButton!
    var statusLabel: UILabel!
    var percentagBottomView: UIView!
    var percentageView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.gray
        
        testButton = UIButton()
        self.addSubview(testButton)
        testButton.setTitle("开始测速", for: UIControlState.normal)
        testButton.addTarget(self, action: #selector(DownloadTestView.downloadButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        testButton.layer.cornerRadius = 2
        testButton.layer.borderWidth = 1
        testButton.layer.borderColor = UIColor.black.cgColor
        
        percentagBottomView = UIView()
        self.addSubview(percentagBottomView)
        percentagBottomView.layer.cornerRadius = 2
        percentagBottomView.layer.borderWidth = 1
        percentagBottomView.layer.borderColor = UIColor.black.cgColor
        
        percentageView = UIView()
        percentagBottomView.addSubview(percentageView)
        percentageView.backgroundColor = UIColor.orange
        
        statusLabel = UILabel()
        self.addSubview(statusLabel)
        statusLabel.textAlignment = NSTextAlignment.center
        statusLabel.layer.cornerRadius = 2
        statusLabel.layer.borderWidth = 1
        statusLabel.layer.borderColor = UIColor.black.cgColor
        
        
        statusLabel.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self)?.setOffset(180.0)
            maker?.leading.equalTo()(self)?.setOffset(100.0)
            maker?.trailing.equalTo()(self)?.setOffset(-100.0)
            maker?.height.setOffset(45.0)
        }
        
        percentagBottomView.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.statusLabel.mas_bottom)?.setOffset(55.0)
            maker?.leading.equalTo()(self)?.setOffset(45.0)
            maker?.trailing.equalTo()(self)?.setOffset(-45.0)
            maker?.height.setOffset(20.0)
        }
        
        percentageView.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.percentagBottomView.mas_top)?.setOffset(0)
            maker?.leading.equalTo()(self.percentagBottomView.mas_leading)?.setOffset(0)
            maker?.trailing.equalTo()(self.percentagBottomView.mas_trailing)?.setOffset(0)
            maker?.bottom.equalTo()(self.percentagBottomView.mas_bottom)?.setOffset(0)
        }
        
        testButton.mas_makeConstraints { (maker: MASConstraintMaker?) in
            maker?.top.equalTo()(self.percentagBottomView.mas_bottom)?.setOffset(55.0)
            maker?.leading.equalTo()(self)?.setOffset(45.0)
            maker?.trailing.equalTo()(self)?.setOffset(-45.0)
            maker?.height.setOffset(65.0)
        }
        
    }
    
    
    var link: CADisplayLink!
    var completedUnitCountBefore = 0
    var completedUnitCountAfter = 0
    var actionStatus = Bool()
    var downloads: DownloadRequest!
    
    //MARK:     
    func downloadButtonAction(sender: UIButton) -> Void {
        if !actionStatus {
            testButton.setTitle("暂停", for: UIControlState.normal)
            actionStatus = !actionStatus
            
            link = CADisplayLink.init(target: self, selector: #selector(DownloadTestView.timerAction(sender:)))
            link.frameInterval = 60
            link.add(to: RunLoop.main, forMode: RunLoopMode.commonModes)
            downloads = download("http://sw.bos.baidu.com/sw-search-sp/software/353374073d79e/googlechrome_mac_55.0.2883.95.dmg")
            downloads.downloadProgress { (p: Progress) in
                self.completedUnitCountAfter = Int(p.completedUnitCount)
                let width = WIDTH - 90.0
                let t = width * CGFloat(p.fractionCompleted)
                DispatchQueue.main.async {
                    self.percentageView.mas_updateConstraints({ (maker: MASConstraintMaker?) in
                        maker?.leading.equalTo()(self.percentagBottomView.mas_leading)?.setOffset(t)
                    })
                    self.percentageView.layoutIfNeeded()
                }
                
                //            self.percentageView.center = CGPoint(x: x_change, y: self.percentageView.center.y)
                //            print("完成\(p.completedUnitCount)")
                //            print("全部\(p.totalUnitCount)")    //bytes 字节
                //            print("百分比\(p.fractionCompleted)")
                }.responseData { (response: DownloadResponse<Data>) in
//                    print(response)
                    self.testButton.setTitle("开始测速", for: UIControlState.normal)
                    self.link.invalidate()
                    self.actionStatus = !self.actionStatus
                    self.completedUnitCountBefore = 0
                    self.completedUnitCountAfter = 0
            }
        } else {
            downloads.cancel()
            testButton.setTitle("开始测速", for: UIControlState.normal)
            link.invalidate()
            actionStatus = !actionStatus
            completedUnitCountBefore = 0
            completedUnitCountAfter = 0
        }
    }
    
    func timerAction(sender: CADisplayLink) -> Void {
        let count = completedUnitCountAfter - completedUnitCountBefore
        completedUnitCountBefore = completedUnitCountAfter
        let changes = Double(count) / 1024.0 / 1024.0
        self.statusLabel.text = String(format: "%.2f", changes) + " M/s"
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
