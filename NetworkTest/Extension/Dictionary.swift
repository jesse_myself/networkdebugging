//
//  Dictionary.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/16.
//  Copyright © 2017年 vsea. All rights reserved.
//

import Foundation


extension Dictionary {
    
    mutating func append(dictionary: Dictionary?) -> Void {
        if dictionary != nil {
            for (key, value) in dictionary! {
                self.updateValue(value, forKey: key)
            }
        }
    }
    
}


extension String {
    mutating func dada() -> Void {
        self.append("1")
    }
}
