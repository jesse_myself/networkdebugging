//
//  ClosureExtension.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/14.
//  Copyright © 2017年 vsea. All rights reserved.
//

import Foundation


typealias notParamsClosure = () -> Void

typealias oneStringParamClosure = (_ str: String) -> Void

typealias urlAndParamsClosure = (_ url: String, _ params: [String: String]) -> Void
