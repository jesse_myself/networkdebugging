//
//  AppDelegate.swift
//  NetworkTest
//
//  Created by jesse Gu on 2017/2/7.
//  Copyright © 2017年 vsea. All rights reserved.
//

import UIKit
import Alamofire
import Masonry


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow(frame: BOUNDS)
        let rootVC = HomeVC()
        let navVC = UINavigationController(rootViewController: rootVC)
        self.window!.rootViewController = navVC
        self.window!.makeKeyAndVisible()
        
        
        
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


let WIDTH = UIScreen.main.bounds.size.width
let HEIGHT = UIScreen.main.bounds.size.height
let BOUNDS = UIScreen.main.bounds


let FFFFFF = UIColor.white
let oooooo = UIColor.black

let ooB9FF = UIColor(red: 0, green: 185, blue: 255, alpha: 0)




//MARK: Masonry

/*
 /*  maker!.top.equalTo()(self.view)!.setOffset(100)                向上约束100， 需要写成 100
 *  maker!.bottom.equalTo()(self.view)!.setOffset(-100)            向下约束100， 需要写成 -100
 *  maker!.leading.equalTo()(self.view)!.setOffset(100)            向左约束100， 需要写成 100
 *  maker!.trailing.equalTo()(self.view)!.setOffset(100.0)         向右约束100， 需要写成 -100
 
 *  maker!.height.setOffset(100.0)                                 设置高度100， 需要写成 100
 
 *  maker!.centerX.equalTo()(self.view)                            剧中
 
 */
 
 */



//MARK:
/* @escaping
 * 这是由于函数参数的默认行为的改变。
 * 在Swift 3（特别是Xcode 8 beta 6附带的版本）之前，他们默认使用@escaping。  你必须将它们标记为@noescape，以防止它们被存储或捕获，因此保证它们调用函数后退出。
 * 然而，现在@noescape是默认的 - 现在你必须将函数参数标记为@escaping告诉编译器他们可以存储或捕获。
 *
 *** 可以直接在函数本身中调用非转义函数参数，或者在非转义闭包中捕获时调用它。 在这种情况下，当我们处理异步代码时，不能保证异步函数参数（因此完成函数）在fetchData退出之前被调用，因此必须是@escaping
 */



//MARK: 
/* @objc
 * 选择器是Objective-C的一个特性，只能用于暴露给动态Obj-C运行时的方法。 你不能有一个选择器到一个纯Swift方法。
 * 必须使用@objc属性来表示你希望这个方法暴露给Obj-C，以便它可以用Obj-C选择器调用。
 * 
 *
 */








